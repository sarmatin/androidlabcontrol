package ru.sarmatin.androidlabcontrolwork.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.raizlabs.android.dbflow.config.FlowManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sarmatin.androidlabcontrolwork.R;
import ru.sarmatin.androidlabcontrolwork.adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {


    @Bind(R.id.viewpager)ViewPager mViewPager;
    @Bind(R.id.tabs)TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        FlowManager.init(this);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(){
       ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new KazanFragment(), "Казань");
        adapter.addFragment(new MoscowFragment(), "Москва");
        mViewPager.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mViewPager != null){
            setupViewPager();
            mTabLayout.setupWithViewPager(mViewPager);
        }
    }
}
