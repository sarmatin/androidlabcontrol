package ru.sarmatin.androidlabcontrolwork.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sarmatin.androidlabcontrolwork.R;
import ru.sarmatin.androidlabcontrolwork.database.Day;
import ru.sarmatin.androidlabcontrolwork.model.DayTemp;

/**
 * Created by antonsarmatin on 15.04.16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Day> dayTemps;

    public RecyclerViewAdapter(List<Day> dayTemps) {
        this.dayTemps = dayTemps;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Log.d("wLog", "onBindVH:" + i);
        Day dayTemp = dayTemps.get(i);
        Log.d("wLog", dayTemp.getDate());
        Log.d("wLog", dayTemp.getAverage());
                Date date = new Date(Long.parseLong(dayTemp.getDate()) * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        String formattedDate = sdf.format(date);
        viewHolder.date.setText(formattedDate);
        viewHolder.average.setText("Cредняя температура: " + dayTemp.getAverage());
        viewHolder.max.setText("Максимальная температура: " + dayTemp.getMax());
        viewHolder.min.setText("Минимальная температура: " + dayTemp.getMin());
        viewHolder.wSpeed.setText("Скорость ветра: " + dayTemp.getSpeed());
    }

    @Override
    public int getItemCount() {
        return dayTemps.size();
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.average)
        TextView average;
        @Bind(R.id.max)
        TextView max;
        @Bind(R.id.min)
        TextView min;
        @Bind(R.id.wSpeed)
        TextView wSpeed;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
