package ru.sarmatin.androidlabcontrolwork.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sarmatin.androidlabcontrolwork.R;
import ru.sarmatin.androidlabcontrolwork.adapter.RecyclerViewAdapter;
import ru.sarmatin.androidlabcontrolwork.database.DayList;
import ru.sarmatin.androidlabcontrolwork.service.RequestService;



/**
 * Created by antonsarmatin on 15.04.16.
 */
public class MoscowFragment extends Fragment {
    private MoscowFragmentListener mListener;
    @Bind(R.id.recyclerView)RecyclerView recyclerView;
    private DayList info;
    private RequestServiceBroadcastReceiver receiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_city, container, false);
        ButterKnife.bind(this, v);

        startService();
        registerReceiver();

        return v;
    }



    private void startService(){
        Intent intentRequestService = new Intent(getActivity(),RequestService.class);
        intentRequestService.putExtra("city_id", "524901");
        Context context =  MoscowFragment.this.getActivity();
        context.startService(intentRequestService);
    }

    private void registerReceiver(){
        receiver = new RequestServiceBroadcastReceiver();
        IntentFilter filter = new IntentFilter(RequestService.ACTION_REQUESTSERVICE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        Context context =  MoscowFragment.this.getActivity();
        context.registerReceiver(receiver, filter);
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        try {
//            mListener = (KazanFragmentListener) getActivity();
//        } catch (ClassCastException e) {
//            throw new ClassCastException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface MoscowFragmentListener {
        void onFragmentInteraction();
    }

    public class RequestServiceBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            info = intent.getParcelableExtra("524901");
            Log.d("wLog", "onRecieveMoscow");
            if(info !=null && info.getAnswerCode() == 0) {
                RecyclerViewAdapter adapter = new RecyclerViewAdapter(info.getList());
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(layoutManager);
            }
        }
    }


}
