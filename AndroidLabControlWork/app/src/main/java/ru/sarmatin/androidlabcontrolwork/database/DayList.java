package ru.sarmatin.androidlabcontrolwork.database;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonsarmatin on 15.04.16.
 */
public class DayList implements Parcelable{

    private List<Day> list;
    private int answerCode = 0;



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(list);
        dest.writeInt(answerCode);
    }

    private DayList(Parcel parcel){
        list = new ArrayList<>();
        parcel.readList(list, null);
        answerCode = parcel.readInt();

    }

    public List<Day> getList() {
        return list;
    }

    public void setList(List<Day> list) {
        this.list = list;
    }

    public int getAnswerCode() {
        return answerCode;
    }

    public void setAnswerCode(int answerCode) {
        this.answerCode = answerCode;
    }

    public DayList(List<Day> list){
        this.list = list;
    }

    public static final Creator<DayList> CREATOR = new Creator<DayList>() {
        // распаковываем объект из Parcel
        public DayList createFromParcel(Parcel in) {
            return new DayList(in);
        }

        public DayList[] newArray(int size) {
            return new DayList[size];
        }
    };
}
