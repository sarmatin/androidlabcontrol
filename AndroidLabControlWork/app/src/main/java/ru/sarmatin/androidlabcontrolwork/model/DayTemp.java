package ru.sarmatin.androidlabcontrolwork.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Антон on 08.03.2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class DayTemp implements Parcelable{


    @JsonProperty("dt")
    private String date;

    @JsonProperty("temp")
    private Temperature temperature;

    @JsonProperty("speed")
    private Double speed;

    @JsonCreator
    public DayTemp(@JsonProperty("dt")String date,@JsonProperty("temp") Temperature temperature,@JsonProperty("speed") Double speed ) {
        this.date = date;
        this.temperature = temperature;
        this.speed = speed;
    }

    protected DayTemp(Parcel in) {
        date = in.readString();
    }

    public static final Creator<DayTemp> CREATOR = new Creator<DayTemp>() {
        @Override
        public DayTemp createFromParcel(Parcel in) {
            return new DayTemp(in);
        }

        @Override
        public DayTemp[] newArray(int size) {
            return new DayTemp[size];
        }
    };

    public Temperature getTemperature() {

        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
    }
}
