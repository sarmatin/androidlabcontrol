package ru.sarmatin.androidlabcontrolwork.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by antonsarmatin on 10.03.16.
 */
@JsonIgnoreProperties(ignoreUnknown =  true)
public class Temperature implements Serializable {


    @JsonProperty("day")
    private Double day;

    @JsonProperty("max")
    private Double max;

    @JsonProperty("min")
    private Double min;




    public Double getDay() {
        return day;
    }

    public void setDay(Double day) {
        this.day = day;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    @JsonCreator
    public Temperature(@JsonProperty("day")Double day,@JsonProperty("max") Double max,@JsonProperty("min") Double min) {
        this.day = new BigDecimal(day).setScale(1, RoundingMode.HALF_UP).doubleValue();
        this.max = new BigDecimal(max).setScale(1, RoundingMode.HALF_UP).doubleValue();
        this.min =new BigDecimal(min).setScale(1, RoundingMode.HALF_UP).doubleValue();

    }
}
