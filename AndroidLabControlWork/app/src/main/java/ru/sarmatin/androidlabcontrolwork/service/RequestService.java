package ru.sarmatin.androidlabcontrolwork.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import ru.sarmatin.androidlabcontrolwork.database.Day;
import ru.sarmatin.androidlabcontrolwork.database.DayList;
import ru.sarmatin.androidlabcontrolwork.model.DayTemp;
import ru.sarmatin.androidlabcontrolwork.model.ForecastInfo;
import ru.sarmatin.androidlabcontrolwork.network.SessionRestManager;

/**
 * Created by antonsarmatin on 15.04.16.
 */
public class RequestService extends IntentService {

    public static final String ACTION_REQUESTSERVICE = "requestService";
    public static String EXTRA_KEY_OUT = "requestServiceOut";
    private String cityCode="0";

    public RequestService() {
        super("serviceForecast");
    }

    public void onCreate() {
        super.onCreate();
        Log.d("wLog", "startService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ForecastInfo forecastInfo = new ForecastInfo();
        cityCode = intent.getStringExtra("city_id");
        List<Day> dayList = new ArrayList<>();
        try{
            forecastInfo =  SessionRestManager.getInstance().getRest().getForecastByCityId(cityCode,"metric", "14");
            for(DayTemp day : forecastInfo.getDayTempList()){
                Day tDay = new Day();
                tDay.setDate(day.getDate());
                tDay.setAverage(String.valueOf(new BigDecimal(((day.getTemperature().getMax() + day.getTemperature().getMin()) / 2)).setScale(1, RoundingMode.HALF_UP).doubleValue()));
                tDay.setMax(day.getTemperature().getMax().toString());
                tDay.setMin(day.getTemperature().getMin().toString());
                tDay.setSpeed(day.getSpeed().toString());
                dayList.add(tDay);
            }


        } catch (RetrofitError error) {
            switch (error.getKind()) {
                case NETWORK:
                    forecastInfo.setAnswerCode(1);
                    break;
                case HTTP:
                    forecastInfo.setAnswerCode(2);
                    break;
                case CONVERSION:
                    forecastInfo.setAnswerCode(3);
                    break;
                case UNEXPECTED:
                    forecastInfo.setAnswerCode(4);
                    break;
            }
        }finally {
            DayList list = new DayList(dayList);
            Intent response = new Intent();
            response.setAction(ACTION_REQUESTSERVICE);
            response.addCategory(Intent.CATEGORY_DEFAULT);
            response.putExtra(cityCode, list);
            Log.d("wLog", "requestService");
            sendBroadcast(response);
        }

    }
}
