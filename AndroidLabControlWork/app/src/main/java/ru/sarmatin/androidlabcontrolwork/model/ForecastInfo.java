package ru.sarmatin.androidlabcontrolwork.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Антон on 08.03.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastInfo implements Parcelable {

    @JsonProperty("list")
//    @JsonDeserialize(as=ArrayList.class, contentAs=DayTemp.class)
    private List<DayTemp> dayTempList;



    private int answerCode;

    public List<DayTemp> getDayTempList() {
        return dayTempList;
    }

    public void setDayTempList(List<DayTemp> dayTempList) {
        this.dayTempList = dayTempList;
    }

    public int getAnswerCode() {
        return answerCode = 0;
    }

    public void setAnswerCode(int answerCode) {
        this.answerCode = answerCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dayTempList);
        dest.writeInt(answerCode);
    }

    @JsonCreator
    public ForecastInfo(@JsonProperty("list")List<DayTemp> dayTempList) {
        this.dayTempList = dayTempList;
    }

    public ForecastInfo() {
    }



    private ForecastInfo(Parcel parcel) {
        dayTempList = new ArrayList<>();
        parcel.readList(dayTempList, null);
        answerCode = parcel.readInt();
    }

    public static final Creator<ForecastInfo> CREATOR = new Creator<ForecastInfo>() {
        // распаковываем объект из Parcel
        public ForecastInfo createFromParcel(Parcel in) {
            return new ForecastInfo(in);
        }

        public ForecastInfo[] newArray(int size) {
            return new ForecastInfo[size];
        }
    };
}


