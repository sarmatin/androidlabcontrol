package ru.sarmatin.androidlabcontrolwork.network;
import retrofit.http.GET;
import retrofit.http.Query;
import ru.sarmatin.androidlabcontrolwork.model.ForecastInfo;

public interface WeatherRest {
//551487
    @GET("/data/2.5/forecast/daily")
    ForecastInfo getForecastByCityId(@Query("id") String cityId, @Query("units") String units, @Query("cnt") String days);




}