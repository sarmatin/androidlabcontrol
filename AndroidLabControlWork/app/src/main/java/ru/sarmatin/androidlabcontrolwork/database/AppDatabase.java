package ru.sarmatin.androidlabcontrolwork.database;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.Model;

/**
 * Created by antonsarmatin on 15.04.16.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase  {

    public static final String NAME = "AppDatabase";

    public static final int VERSION = 1;
}