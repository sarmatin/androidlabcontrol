package ru.sarmatin.androidlabcontrolwork.network;


import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import ru.sarmatin.androidlabcontrolwork.BuildConfig;
import ru.sarmatin.androidlabcontrolwork.Config;
import ru.sarmatin.androidlabcontrolwork.utils.JacksonConverter;


public class SessionRestManager {

    private static volatile SessionRestManager sInstance;


    private SessionRestManager() {
    }

    public static SessionRestManager getInstance() {
        if (sInstance == null)
            synchronized (SessionRestManager.class) {
                if (sInstance == null)
                    sInstance = new SessionRestManager();
            }
        return sInstance;
    }


    private final RequestInterceptor REQUEST_INTERCEPTOR = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addQueryParam("appid", Config.APPLICATION_ID);
            request.addHeader("Accept", "application/json");
        }
    };


    private final RestAdapter.Builder REST_ADAPTER_BUILDER = new RestAdapter.Builder()
            .setEndpoint(Config.WEATHER_ENDPOINT_RELEASE)
            .setConverter(new JacksonConverter())
            .setRequestInterceptor(REQUEST_INTERCEPTOR)
            .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL
                    : RestAdapter.LogLevel.NONE);

    private final RestAdapter REST_ADAPTER = REST_ADAPTER_BUILDER.build();

    public WeatherRest getRest() {
        return REST_ADAPTER.create(WeatherRest.class);
    }

}
