package ru.sarmatin.androidlabcontrolwork.database;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;


/**
 * Created by antonsarmatin on 15.04.16.
 */

@Table(database = AppDatabase.class)
public class Day extends BaseModel implements Serializable {

    @Column
    @PrimaryKey(autoincrement = true)
    private long id;

    @Column
    private String date;
    @Column
    private String average;
    @Column
    private String max;
    @Column
    private String min;
    @Column
    private String speed;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }





}
